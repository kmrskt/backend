var request = require('request-promise')
var Promise = require('bluebird')
var mongoConnection = require(__base + '/service/mongodbConnectionService');
var ObjectId = require('mongodb').ObjectID;

function findDetails(collection, dbs) {
    return new Promise(function(resolve, reject) {
            mongoConnection.getConnection()
            .then(function(client) {

                const db = client.db(dbs);
                const col = db.collection(collection);
                col.find().toArray(function(err, docs) {
                    if (err) {
                        console.log(err);
                        reject(err)
                    }
                    resolve(docs)

                    client.close();

                });

            }).catch(function(err) {
                console.log(err)
                reject(err)
                client.close();


            })
    })
}

function upvote(obj,collection, dbs) {
    return new Promise(function(resolve, reject) {
            mongoConnection.getConnection()
            .then(function(client) {
                const db = client.db(dbs);
                const col = db.collection(collection);
                db.collection(collection).update( { "_id":new ObjectId(obj) },{ $inc: { votecount: 1 }},function(err, res) {
                  if (err)
                  {
                    reject(err);
                    throw err;
                    client.close();
                  }
                  console.log("1 document updated");
                  resolve("updated")
                  client.close();
                });
              });
            });
      }


      function saveData(comment,time,collection, dbs) {
          return new Promise(function(resolve, reject) {
                  mongoConnection.getConnection()
                  .then(function(client) {
                      const db = client.db(dbs);
                      const col = db.collection(collection);
                      var myobj = { "comment": comment, "votecount": 0 ,"time":time };
                      db.collection(collection).insertOne( myobj,function(err, res) {
                        if (err)
                        {
                          reject(err);
                          throw err;
                          client.close();
                        }
                        resolve("inserted")
                        client.close();
                      });

                    });
                  });
            }

module.exports = { findDetails: findDetails ,upvote:upvote,saveData:saveData}
