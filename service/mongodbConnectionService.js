const MongoClient = require('mongodb').MongoClient;
var Promise = require('bluebird')

//const url = DBURL='mongodb://localhost:27017';
const url = process.env.DBURL;

// Database Name
const dbName = 'post';

// Use connect method to connect to the server

function getConnection() {

    return new Promise(function(resolve, reject) {
        MongoClient.connect(url, function(err, client) {

            if (err) { reject(err) }
            resolve(client)
            console.log("success");
        })
    });
}

module.exports = {  getConnection:getConnection }
