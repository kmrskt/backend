
FROM node:10.9.0-alpine


ENV PORT 3003
EXPOSE 3003


WORKDIR /usr/src/app


COPY package.json .
RUN npm install -g
COPY . .

CMD NODE_TLS_REJECT_UNAUTHORIZED=0 node bin/www
