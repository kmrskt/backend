var Promise = require('bluebird');
var detailService = require(__base + '/service/detail');


async function findDetails(req)
{
collection="details";
dbs="post";

var result = await detailService.findDetails(collection,dbs);
return result;
}

async function upvote(req)
{
collection="details";
dbs="post";
id=req.body.id;
var result = await detailService.upvote(id,collection,dbs);
return result;
}

async function saveData(req)
{
collection="details";
dbs="post";
id=req.body.id;
comment=req.body.comment;
time= new Date();
var result = await detailService.saveData(comment,time,collection,dbs);
return result;
}

module.exports = { findDetails:findDetails,upvote:upvote,saveData:saveData }
