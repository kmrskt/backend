var express = require('express');
var router = express.Router();
var detailBl = require(__base + '/bl/detail');

router.get('/getData',
        async function(req, res) {
                    try {
                      
                       result =  await detailBl.findDetails(req);
                       res.status(200).send(result);
                        }
                    catch (err)
                    {
                        console.error(err);
                        res.status(500).send(err);
                    }
        });

router.post('/upvote',
                async function(req, res) {
                            try {
                              //res.status(200).send("result");
                               result =  await detailBl.upvote(req);
                               res.status(200).send(result);
                                }
                            catch (err)
                            {
                                console.error(err);
                                res.status(500).send(err);
                            }
                });


router.post('/saveData',
                          async function(req, res) {
                                            try {
                                              //res.status(200).send("result");
                                               result =  await detailBl.saveData(req);
                                               res.status(200).send(result);
                                                }
                                            catch (err)
                                            {
                                                console.error(err);
                                                res.status(500).send(err);
                                            }
        });


module.exports = router;
