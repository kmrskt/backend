
var express = require('express');
var app = express();

var detailController = require(__base + '/controller/detail.js');


app.use('/detail', detailController);


module.exports = app;
